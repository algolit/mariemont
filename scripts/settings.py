# -*- coding: utf-8 -*-
import os.path

DEBUG = True
BASEPATH = os.path.dirname(os.path.realpath(__file__))
DATABASE = os.path.join(BASEPATH, '../data/mariemont.db')
HPGL_CACHEDIR = os.path.join(BASEPATH, '../data/plots')

PLOT_PAGE = { 'top': 10430, 'right': 7400, 'bottom': 430, 'left': 200 }
PLOT_HEADER_TEXT = 'Une Anthologie'
PLOT_FOOTER_TEXT = 'Bye Bye Future! Musée royal de Mariemont, 2020\r\nGijs de Heij & Anaïs Berck'

PLOT_BODY_CHARACTER_SIZE = (0.3, 0.385)
PLOT_FOOTER_CHARACTER_SIZE = (0.2, 0.275)

BASEURL = '/api'
VIRTUAL_PLOTTER = True

THERMAL_HEADER_TEXT = 'Une Anthologie'
THERMAL_FOOTER_TEXT = 'Bye Bye Future! Musée Royal de Mariemont\nGijs de Heij & Anaïs Berck'
THERMAL_CACHEDIR = os.path.join(BASEPATH, '../data/thermal')