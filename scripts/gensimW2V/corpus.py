from gensim import utils
import os.path

class MariemontCorpus(object):
    """An interator that yields sentences (lists of str)."""

    def __iter__(self):
        # corpus_path = datapath('lee_background.cor')
        base = os.path.dirname(os.path.realpath(__file__))
        corpus_path = os.path.join(base, 'data_sentences.txt')
        for line in open(corpus_path, 'r'):
            # assume there's one document per line, tokens separated by whitespace
            yield utils.simple_preprocess(line)
