Start of using the gensim word2vec implementation.

## Train
Train the model using the `train_w2v_model.py`

## Use / inspect
With the script `mariemont_inspectory.py` it is possible to explore the model through the most similar function.