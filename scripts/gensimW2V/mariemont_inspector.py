# from gensim.models import Word2Vec
from gensim.models import KeyedVectors

print('Loading model')
# model = Word2Vec.load('mariemont2vec.model')
model = KeyedVectors.load('mariemont.wv')

while True:
  sw = str(input('Search word: ')).lower()

  if sw:
    print('\nMost similar to: {}'.format(sw))
    try:
      for word, distance in model.most_similar(sw):
        print('  {} → {}'.format(word, float(distance)))
      print('\n')
    except:
      print('The word is not part of the model vocabulary.\n')