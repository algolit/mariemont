from gensim.test.utils import datapath
import gensim.models
from corpus import MariemontCorpus

sentences = MariemontCorpus()
print("Starting training")
model = gensim.models.Word2Vec(sentences=sentences, size=100, window=5, min_count=1, workers=4)
print("Done. Saving")
model.save('mariemont.model')
model.wv.save('mariemont.wv')