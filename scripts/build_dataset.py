
def build_dataset(words):
	count = [['UNK', -1]]
	count.extend(collections.Counter(words).most_common(vocabulary_size - 1))
	dictionary = dict()
	for word, _ in count:
		dictionary[word] = len(dictionary)
	data = list()

	# Custom Algolit addition (logging disregarded words)
	disregarded = list()

	unk_count = 0
	for word in words:
		if word in dictionary:
			index = dictionary[word]
		else:
			index = 0  # dictionary['UNK']
			unk_count += 1

			# Custom Algolit addition
			disregarded.append(word)

		data.append(index)
	count[0][1] = unk_count
	reverse_dictionary = dict(zip(dictionary.values(), dictionary.keys()))
	return data, count, dictionary, reverse_dictionary, disregarded

def read_input_text(trainingset):
	with open(trainingset, 'r', buffering=4048) as source:
		line_cnt = 0
		print('File opened')
		for line in source:
			if line:
				yield line.strip()
				# wordlist =  line.split(' ')
				# for word in wordlist:
				# 	yield word
			line_cnt += 1

			if (line_cnt % 1000000) == 0:
				print(line_cnt)
		# lines = source.readlines()
		
		# print('Read )
		# for line in lines:
		# 	# line = line.decode('utf8')
		# 	wordlist = word_tokenize(line)
		# 	for word in wordlist:
		# 		yield word

