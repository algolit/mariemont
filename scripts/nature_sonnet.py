from db import connect, find_one_word, find_words
from sonnets import generate_queneau_sonnet
import re
import random
import sys

"""
  word CHARACTER(50),
  lemma CHARACTER(50),
  tag CHARACTER(5),
  syllable_count INTEGER,
  rhyme_end CHARACTER(5),
  nature_score FLOAT,
  has_nature_score BOOLEAN
"""

vowels = 'aeiouyáéíóúýàèìòùỳâêîôûŷäëïöüÿ'

def load_queneau_replacement_words ():
  with open('../data/queneau_words.txt', 'r') as source:
    return [l.strip() for l in source]

def find_replacement(conn, word, should_rhyme=False, nature_score=None):
  # Find information about this word
  tag = 'NOM'
  word_data = find_one_word(conn, [('tag', tag), ('word', word)])
  if not word_data:
    # Try without POS tag
    word_data = find_one_word(conn, [('word', word)])
    # print('Coud not find word: {}'.format(word))
    if word_data:
      tag = word_data['tag']
  if word_data:
    # Find words with an equal amount of syllables and the same tag
    parameters = [ ('tag', tag), 
      ('gender', word_data['gender']), 
      ('plural', word_data['plural']), 
      ('syllable_count', word_data['syllable_count'])]

    if nature_score:
      parameters.append(('nature_score', 'BETWEEN', '{} AND {}'.format(nature_score[0], nature_score[1])))
      parameters.append(('has_nature_score', 1))

    if should_rhyme:
      parameters.append(('rhyme_end', word_data['rhyme_end']))

    parameters.append(('word', '!=', word))

    words = find_words(conn, parameters, limit=10)

    if words:
      return [w['word'] for w in words[:10]]
  
  return None

words_to_replace = load_queneau_replacement_words()

def generate_nature_sonnet (score):
  # generate a sonnet

  conn = connect()

  sonnet = generate_queneau_sonnet()
  new_sonnet = []

  # per line replace candidate words with a word that is similar
  for line in sonnet:
    new_line = []
    should_rhyme = True
    line_words = line.split(' ')
    line_words.reverse()
    for word in line_words:
      word = word.lower()

      if word in words_to_replace:
        replacements = None
        # start = 8
        # i = 0

        # # First try to find a replacement within the nature selection        
        # while not replacements and i <= 5:
        #   replacements = find_replacement(word, should_rhyme, nature_score=(start, 9999))  
        #   start -= 1
        #   i += 1

        window = 0.5

        # First try to find a replacement within the nature selection        
        while not replacements and window <= 10:
          replacements = find_replacement(conn, word, should_rhyme, nature_score=(score-window, score+window))  
          window += .33


        # If we can't find a replacement within that range select
        # based on syllable count and rhyme preference
        if not replacements:
          replacements = find_replacement(conn, word, should_rhyme)

          # If still no match and we where looking for a rhyming word
          # drop rhyme preference
          if not replacements and should_rhyme:
            replacements = find_replacement(conn, word, should_rhyme=False)
        
          # Finally give up
          if not replacements:
            replacements = ['NO REPLACEMENTS']

        new_line.append([word, random.choice(replacements)])
        should_rhyme = False
      else:
        if re.match(r'\w+', word):
          should_rhyme = False

        # this word is le or la
        # previous word is a replacement
        # previous word starts with a vowel
        if len(new_line) > 0 \
          and (word == 'le' or word == 'la') \
          and len(new_line[-1]) > 1 \
          and new_line[-1][1][0].lower() in vowels:
            new_line[-1][1] = 'l\''+ new_line[-1][1]
        # replace de & du for d' if replacement word starts with a vowel
        elif len(new_line) > 0 \
          and (word == 'de' or word == 'du') \
          and len(new_line[-1]) > 1 \
          and new_line[-1][1][0].lower() in vowels:
            new_line[-1][1] = 'd\''+ new_line[-1][1]
        # Replace sa for son if replacement word starts with a vowel
        elif len(new_line) > 0 \
          and (word == 'sa') \
          and len(new_line[-1]) > 1 \
          and new_line[-1][1][0].lower() in vowels:
            new_line.append(('son',))
        else:
          new_line.append((word,))

    new_line[-1] = [w.capitalize() for w in new_line[-1]]
    new_line.reverse()
    new_sonnet.append(new_line)
  
  return new_sonnet


# for debug take one line, see how many options there are
# what is the correct pos-tag?

if __name__ == '__main__':

  if len(sys.argv) > 1:
    score = float(sys.argv[1])
  else:
    score = 0.0

  new_sonnet = generate_nature_sonnet(score)
  
  print ('A generated sonnet with nature scorce: {}'.format(score))
  
  for line in new_sonnet:
    output = []
    for word in line:
      if len(word) > 1:
        output.append('{replacement} ({old})'.format(old=word[0], replacement=word[1]))
      else:
        output.append(word[0])

    print(' '.join(output))