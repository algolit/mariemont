from pyxlsb import open_workbook
import csv 

with open_workbook('Lexique383.xlsb') as wb:
	for sheetname in wb.sheets:
		with wb.get_sheet(sheetname) as sheet:
			with open(r'Lexique.csv', 'w') as destination:
				writer = csv.writer(destination)
				for row in sheet.rows():
					values = [r.v for r in row]  # retrieving content
						writer.writerow(values)
					