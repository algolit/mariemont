"""
-- word: str
-- lemma: str
-- tag: str
-- syllable_count: int
-- rhyme_end: str
-- nature_score: float
"""

import csv
from db import connect, create_table, create_indexes, insert_many_words, drop_table_and_indexes
from nature_classifier import predict

def load_lexicon(filename):
  lexicon = []
  with open(filename, encoding='utf-8') as infile:
    for line in infile:
      lexicon.append(line.strip())
  return lexicon

nature_words = load_lexicon('../data/synonymes_nature_clean.txt')
technology_words = load_lexicon('../data/synonymes_technique_clean.txt')


def extract_rhyme (syllables):
  ## To do more intelligent
  consonants = ['b', 'c', 'd', 'f', 'g', 'G', 'h', 'j', 'k', 'l', 'm', 'n', 'N', 'p', 'r', 'R', 's', 'S', 't', 'v', 'w', 'x', 'z', 'Z']
  vowels = ['a', 'e', 'i', 'o', 'u', 'y', 'A', 'E', 'I', 'O', 'U', 'Y', '1', '2', '5', '8', '9', '0', '°', '@', '§']
  
  chars = list(syllables.split('-')[-1])
  # print(chars)
  rhyme = []

  while len(chars) > 0:
    rhyme.append(chars.pop())
    if rhyme[-1] in vowels:
      rhyme.reverse()
      # print(''.join(rhyme))
      return ''.join(rhyme)
    # elif rhyme[-1] not in consonants:
    #   print(syllables)
    #   print(vowels, rhyme, rhyme[-1] in vowels)
    #   print('UNKNOWN CHAR ', rhyme[-1])
    #   exit()
    # # chars = last.unshift()

  return ''.join(rhyme)



with open('../data/Lexique383_met_alles.csv', 'r') as source:
  reader = csv.reader(source)

  conn = connect()
  drop_table_and_indexes(conn)
  if create_table(conn)  and create_indexes(conn):
    words = []
    unscored = 0
    for idx, row in enumerate(reader):
      if idx > 0:
        if idx % 1000 == 0:
          print(idx)

        # nature_score = 1
        nature_score = predict(row[0])
        has_nature_score = True
        
        if nature_score is None:
          nature_score = predict(row[2])

        if nature_score is None:
          if row[0] in nature_words or row[2] in nature_words:
            print('Found word in nature list: {}'.format(row[0]))
            nature_score = 8
          if row[0] in technology_words or row[2] in technology_words:
            print('Found word in techonology list: {}'.format(row[0]))
            nature_score = -8
          else:
            nature_score = 0
            has_nature_score = False
            unscored += 1
          
        words.append({
          'word': row[0],
          'lemma': row[2],
          'tag': row[3],
          'gender': row[4],
          'plural': True if row[5] == 'p' else False,
          'syllable_count': row[23],
          'rhyme_end': extract_rhyme(row[22]),
          'nature_score': nature_score,
          'has_nature_score': has_nature_score
        })

        
    print('Adding {} words'.format(len(words)))
    print('Unscored words: {}'.format(unscored))
    insert_many_words(conn, words)