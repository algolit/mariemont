from gensim.models import KeyedVectors 
from sonnets import load_verses, generate_sonnet
from pattern.fr import parsetree, parse, Sentence, singularize, pluralize
# from pattern import Sentence

model = KeyedVectors.load('gensimW2V/mariemont.wv')
verses = load_verses('sonnets_queneau.txt')
sonnet = generate_sonnet(verses)
tagged_sonnet = [parse(s, lemmata=True) for s in sonnet]
parsed_sonnet = [Sentence(ts) for ts in tagged_sonnet]

# print(tagged_sonnet, parsed_sonnet)
for l in parsed_sonnet:
  
  for word in l.words:
    print(word.string)
    if word.type[:2] == 'NN':
      sw = word.string
      plural = False
      
      if word.type[-1] == 'S':
        plural = True
        sw = singularize(sw)

      try:
        print(model.most_similar(sw))
      except KeyError: ## Word not in vocabulary
        pass
    
    
    # print('\n')

# for v in new_sonnet:
#   tree = parsetree(v)


# for s in tree:
#   print(s.string)


# for line in new_sonnet:
  # print(line)