import csv

rows = {}

print('Loading CSV')

with open('../data/Lexique.csv', 'r') as file:
	reader = csv.reader(file)
	for row in reader:
		rows[row[0].lower()] = row
		# rows.append(row)
# print(rows)

print('CSV Loaded')

with open('../data/queneau_words.txt', 'r') as source:
	text = source.readlines()
	for word in text:
		word = word.strip().lower()
		if word in rows:
			print('found word:', rows[word])
		else:
			print('Could not find: ', word)
