#!/usr/bin/python 
# -*- coding: utf-8 -*-

import nltk
import codecs
import glob 
import pickle

texts_patt = 'txt/*.txt'
output_file = 'data_sentences.txt'
tokenizer = nltk.data.load('tokenizers/punkt/PY3/french.pickle')
all_sentences = []

with open(output_file, 'wb') as destination:
	for path in glob.glob(texts_patt):
		print('** READING: ', path)

		with codecs.open(path, 'r', "utf-8") as source:
			lines = source.readlines()
			for line in lines:
				sentences = tokenizer.tokenize(line)
				for sentence in sentences:
					if sentence: 
						all_sentences.append(sentence)

	pickle.dump(all_sentences, destination)