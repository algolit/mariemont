import csv
from nature_classifier import predict

with open('../data/Lexique.csv', 'r') as source:
  reader = csv.reader(source)

  with open('../data/Lexique-expanded.csv', 'w') as destination:
    writer = csv.writer(destination)
    for idx, row in enumerate(reader):
      if idx > 0 and idx % 1000 == 0:
        print(idx)

      if idx == 0:
        expanded = row + ['nature_classification']
      else:
        prediction = predict(row[0])
        if prediction is None:
          prediction = 'unk'  
        expanded = row + [prediction]
      writer.writerow(expanded)