#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os.path
import time
import glob
from escpos import printer
from settings import THERMAL_CACHEDIR
from shutil import move
from os import makedirs
import codecs


def find_printer ():
  candidates = glob.glob('/dev/ttyUSB*')

  if candidates:
    return printer.Serial(candidates[0], 19200)
  
  return None

if not os.path.exists(THERMAL_CACHEDIR):
  makedirs(THERMAL_CACHEDIR)

if not os.path.exists(os.path.join(THERMAL_CACHEDIR, 'printed')):
  makedirs(os.path.join(THERMAL_CACHEDIR, 'printed'))

def get_oldest_cachefile():
  files = [path for path in glob.glob(
    os.path.join(THERMAL_CACHEDIR, '*.txt'))]
  if files:
    return sorted(files, key=lambda p: int(os.path.getmtime(p)), reverse=True)[0]
  else:
      return None

while True:
  try:
    thermal_printer = find_printer()

    # Not able to instantiate a printer now. Trying again in 30 seconds.
    if not thermal_printer:
      time.sleep(30)
      continue

    while True:
      cachefile = get_oldest_cachefile()
      if cachefile:
        with open(cachefile, 'r') as h:
          try:
            thermal_printer.set(custom_size=False, height=1, width=1, font='a', flip=True)
            thermal_printer.line_spacing(65)
            lines = h.read().split('\n')
            lines.reverse()
            thermal_printer.text('  ' + '\n  '.join(lines))
            thermal_printer.cut()
          except Exception as e:
            print('An error occured. Clearing printer. Trying again in 30 seconds.')
            print(str(e))
            thermal_printer = None
            time.sleep(1)
            break

        move(cachefile, os.path.join(THERMAL_CACHEDIR, 'printed', os.path.basename(cachefile)))
        

      time.sleep(1)
  except Exception as e:
    print('Could not initiate printer, waiting one minute')
    print(str(e))
    time.sleep(60)