print('Starting import')

from gensim import utils
from gensimW2V.corpus import MariemontCorpus
from sonnets import generate_queneau_sonnet
from nltk.corpus import stopwords as nltk_stopwords
from gensim.models import KeyedVectors
import math


print('Finished import')

# Generate a sonnet
print('Generating sonnet')
sonnet = generate_queneau_sonnet()
# Load MariemontCorpus
print('Loading corpus')
corpus = MariemontCorpus()
# Load stopwords from NLTK
print('Retreiving stopwords')
stopwords = nltk_stopwords.words('french')
print('Loading vectors')
model = KeyedVectors.load('gensimW2V/mariemont.wv')
model.init_sims(replace=True)

def remove_stop_words(sentence, stopwords):
  return [w for w in sentence if w not in stopwords]

print('Measuring distances')
# Pick / loop through verse
for verse in sonnet:
  verse_clean = remove_stop_words(utils.simple_preprocess(verse), stopwords)
  # Loop through sentences in corpus (limit to first 1000?)
  l = 0
  print(verse_clean)
  distances = []
  for corpus_sentence in corpus:
    # print(remove_stop_words(corpus_sentence, stopwords))
    corpus_sentence_clean = remove_stop_words(corpus_sentence, stopwords)
    if corpus_sentence_clean:
      distance = model.wmdistance(verse_clean, corpus_sentence_clean)
      distances.append((corpus_sentence, distance))
    else:
      # print('Only stopwords in this sentence?\n{}\n\n'.format(corpus_sentence))
      pass
    if (l % 10**3) == 0:
      print(l)
      if l >= 10**6:
        break
    l += 1

  break

sorted_sents = sorted(distances, key=lambda l: l[1])

print(verse)
print('Closest sentences:')
# Print 10 closest sentences
for s in sorted_sents[:10]:
  print('{} → {}'.format(' '.join(s[0]), s[1]))

print('Furthest sentences:')
# Print 10 furthest sentences
for s in sorted_sents[-10:]:
  print('{} → {}'.format(' '.join(s[0]), s[1]))


# print(verse)
# print(distances)

# Measure wmdistance (also clean ?)

# def preprocess(sentence):
#     return [w for w in sentence.lower().split() if w not in stop_words]

# sentence_obama = preprocess(sentence_obama)
# sentence_president = preprocess(sentence_president)
