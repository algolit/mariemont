from gensim.models import KeyedVectors
from sklearn.externals import joblib
import numpy as np

print('Loading model')
embeddings = KeyedVectors.load('gensimW2V/mariemont.wv')
classifier = joblib.load('../data/nature_classifier.pkl')

# for key, label in enumerate(model.vocab):
#     vector = model.vectors[key]
#     labels.append(label)
#     values = np.array([float(x) for x in vector], 'f')

# Predicts the nature classification of the given word
def predict (word):
  word = word.lower().strip()
  if word in embeddings.vocab:
    vector = embeddings.vectors[embeddings.vocab[word].index]
    pred_vector = np.array([float(x) for x in vector], 'f')
    predictions = classifier.predict_log_proba([pred_vector])
    
    return predictions[0][1] - predictions[0][0]
  else:
    return None
# model.predict()

if __name__ == '__main__':
  while True:
    sw = str(input('Search word: ')).lower()
    prediction = predict(sw)

    if sw is None:
      print('The word is not part of the model vocabulary.')
    else:
      print(prediction)
    
    print()