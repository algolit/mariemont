# from gensim.models import Word2Vec
from gensim.models import KeyedVectors

print('Loading model')
# model = Word2Vec.load('mariemont2vec.model')
model = KeyedVectors.load('mariemont.wv')


def similarity(inputfile, outputfile):
	synonymes = set()
	with open(inputfile, 'r') as source:
		text = source.readlines()
		for element in text:
			element = element.lower().strip()
			synonymes.add(element)
			print('\nMost similar to: {}'.format(element))
			try:
				for word, distance in model.most_similar(element):
					print('  {} → {}'.format(word, float(distance)))
					synonymes.add(word)
				print('\n')
			except:
				print('The word is not part of the model vocabulary.\n')
	print(synonymes)
	with open(outputfile, 'w') as destination:
		for element in synonymes:
			destination.write(element)
			destination.write('\n')

inputfile = '../../data/technique_mots.txt'
outputfile = '../../data/synonymes_technique_list.txt'
results = similarity(inputfile, outputfile)
