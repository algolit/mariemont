### Following this tutorial in Python3:
### https://gist.github.com/rspeer/ef750e7e407e04894cb3b78a82d66aed#file-how-to-make-a-racist-ai-without-really-trying-ipynb
import numpy as np
import re
import os

from sklearn.linear_model import SGDClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

from gensim.models import KeyedVectors

def load_lexicon(filename):
  lexicon = []
  with open(filename, encoding='utf-8') as infile:
    for line in infile:
      lexicon.append(line.strip())
  return lexicon

embeddings = KeyedVectors.load('gensimW2V/mariemont.wv')

pos_words = load_lexicon('../data/synonymes_nature_clean.txt')
neg_words = load_lexicon('../data/synonymes_technique_clean.txt')
# pos_words = load_lexicon('../data/nature_mots.txt')
# neg_words = load_lexicon('../data/technique_mots.txt')

cutoff = min(len(pos_words), len(neg_words))

pos_words = pos_words[:cutoff-1]
neg_words = neg_words[:cutoff-1]

# print(pos_words[:10])
# print(neg_words[:10])

# Algolit adaption:
# save data to txt file
def export(fn, data):
    outputdir = 'txt/'
    if not os.path.exists(outputdir):
        os.makedirs(outputdir)
    with open(outputdir+fn,'w+') as output:
        output.write(str(data))
        print('\n\n*exported '+fn+'*, with the following content:')
        print(data)

def find_embeddings (model, words):
  labels = []
  vectors = []

  for word in words:
    if word in model.vocab:
      labels.append(word)
      vectors.append(np.array([float(x) for x in model.vectors[model.vocab[word].index]], 'f'))

  return (labels, vectors)

#the data points here are the embeddings of these positive and negative words. 
#We use the Pandas .loc[] operation to look up the embeddings of all the words.
pos_labels, pos_vectors = find_embeddings(embeddings, pos_words) #embeddings.loc[pos_words]
neg_labels, neg_vectors = find_embeddings(embeddings, neg_words)

#Some of these words are not in the GloVe vocabulary, particularly the misspellings such as "fancinating". 
#Those words end up with rows full of NaN to indicate their missing embeddings, so we use .dropna() to remove them.
# pos_vectors = embeddings.loc[pos_words].dropna()
# neg_vectors = embeddings.loc[neg_words].dropna()
print('Nature words: {}'.format(len(pos_vectors)))
print('Technique words: {}'.format(len(neg_vectors)))

'''
Now we make arrays of the desired inputs and outputs. 
The inputs are the embeddings, and the outputs are 1 for positive words and -1 for negative words. 
We also make sure to keep track of the words they're labeled with, so we can interpret the results.
'''
vectors = pos_vectors + neg_vectors
targets = np.array([1 for entry in pos_vectors] + [-1 for entry in neg_vectors])
labels = pos_labels + neg_labels

# print(vectors) # cfr 77/78: matrix of [55 rows x 300 columns] for positive words & [12 rows x 300 columns]
# print(targets) # this is 1 for positive words & -1 for negative words
# print(labels) # these are the words (labels) of the lexicon that are present in the Glove trainingdata

# export('vectors.txt', vectors)
# export('targets.txt', targets)
# export('labels.txt', labels)

'''
Using the scikit-learn train_test_split function, we simultaneously separate the input vectors, 
output values, and labels into training and test data, with 10% of the data used for testing.
'''
train_vectors, test_vectors, train_targets, test_targets, train_labels, test_labels = \
    train_test_split(vectors, targets, labels, test_size=0.1, random_state=0)

'''
Now we make our classifier, and train it by running the training vectors through it for 100 iterations. 
We use a logistic function as the loss, so that the resulting classifier can output the probability 
that a word is positive or negative.
'''
model = SGDClassifier(loss='log', random_state=0, n_iter=1000)
model.fit(train_vectors, train_targets)
#print(model)

'''
We evaluate the classifier on the test vectors. 
It predicts the correct sentiment for sentiment words outside of its training data 95% of the time. 
'''
score = (accuracy_score(model.predict(test_vectors), test_targets))
print(score)

from sklearn.externals import joblib
joblib.dump(model, '../data/nature_classifier.pkl')