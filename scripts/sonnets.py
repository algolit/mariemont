#!/usr/bin/python 
# -*- coding: utf-8 -*-

import codecs
from random import choice

# import sonnets Queneau
def load_verses (path):
	verses = {}

	with codecs.open(path, 'r', 'utf-8') as source:
		text = source.readlines()
		cnt = 0
		for line in text:
			line = line.strip()
			if line:
				if cnt in verses.keys():
				# append the new number to the existing array at this slot
					verses[cnt].append(line)
				else:
					# create a new array in this slot
					verses[cnt] = [line]
			else:
				cnt +=1

	return verses
# print(verses)

def generate_sonnet (verses):
	new_sonnet = []
	for key, value in verses.items(): 
		new_sonnet.append(choice(value))
	return new_sonnet

# Conveinience function. Inefficient as it
# reads the source text with every call!
"""
	Generates a sonnet, returns a list of lines
"""
def generate_queneau_sonnet ():
	return generate_sonnet(load_verses('sonnets_queneau.txt'))

if __name__ == '__main__':
	# Generate random sonnet
	verses = load_verses('sonnets_queneau.txt')
	new_sonnet = generate_sonnet(verses)

	for verse in new_sonnet:
		print(verse)


