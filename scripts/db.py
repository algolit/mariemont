import sqlite3
import settings
from utils import error, debug

INDEXES = [
  ('tag', 'word'),
  ('tag', 'gender', 'plural', 'syllable_count'),
  ('tag', 'gender', 'plural', 'syllable_count', 'nature_score'),
  ('tag', 'gender', 'plural', 'syllable_count', 'rhyme_end'),
  ('tag', 'gender', 'plural', 'syllable_count', 'nature_score', 'rhyme_end'),
  ('tag', 'gender', 'plural', 'syllable_count', 'word'),
  ('tag', 'gender', 'plural', 'syllable_count', 'nature_score', 'word'),
  ('tag', 'gender', 'plural', 'syllable_count', 'rhyme_end', 'word'),
  ('tag', 'gender', 'plural', 'syllable_count', 'nature_score', 'rhyme_end', 'word' )
]

def connect():
  conn = sqlite3.connect(settings.DATABASE)
  conn.row_factory = sqlite3.Row
  return conn

def drop_table_and_indexes(conn):
  cur = conn.cursor()
  try:
    for row in INDEXES:
      cur.execute("DROP INDEX IF EXISTS {}".format('_'.join(row)))
    cur.execute("DROP TABLE IF EXISTS words")
    return True
  except Exception as e:
    conn.rollback()
    error("Could not drop words table {}".format(e))
    return False

def create_table(conn):
  cur = conn.cursor()
  try:
    cur.execute("""CREATE TABLE IF NOT EXISTS words(
      word CHARACTER(50),
      lemma CHARACTER(50),
      tag CHARACTER(5),
      gender CHARACTER(1),
      plural BOOLEAN,
      syllable_count INTEGER,
      rhyme_end CHARACTER(5),
      nature_score FLOAT,
      has_nature_score BOOLEAN
    )""")
    conn.commit()
    return True
  except Exception as e:
    conn.rollback()
    error("Could not create sentences table {}".format(e))
    return False

def create_indexes(conn):
  cur = conn.cursor()
  try:
    for row in INDEXES:
      name = '_'.join(row)
      columns = ', '.join(row)
      q = "CREATE INDEX IF NOT EXISTS {name} ON words ({columns})".format(name=name, columns=columns)
      cur.execute(q)
    conn.commit()
    return True
  except Exception as e:
    conn.rollback()
    error("Could not create indexes {}".format(e))
    return False


def insert_word (conn, word='', lemma='', tag='', gender='', plural=False, syllable_count=0, rhyme_end='', nature_score = 0.0, has_nature_score = False):
  cur = conn.cursor()
  try:
    cur.execute("""INSERT INTO words(
      word,
      lemma,
      tag,
      gender,
      plural,
      syllable_count,
      rhyme_end,
      nature_score,
      has_nature_score)
    VALUES(
      :word,
      :lemma,
      :tag,
      :gender,
      :plural,
      :syllable_count,
      :rhyme_end,
      :nature_score,
      :has_nature_score
    )
    """, {
      'word': word,
      'lemma': lemma,
      'tag': tag,
      'gender': gender,
      'plural': plural,
      'syllable_count': syllable_count,
      'rhyme_end': rhyme_end,
      'nature_score': nature_score,
      'hsa_nature_score': has_nature_score
    })
    conn.commit()
    return True
  except Exception as e:
    conn.rollback()
    error('Could not insert word {}'.format(e))
    return False

def insert_many_words (conn, words=[]):
  cur = conn.cursor()
  try:
    cur.executemany("""INSERT INTO words(
      word,
      lemma,
      tag,
      gender,
      plural,
      syllable_count,
      rhyme_end,
      nature_score,
      has_nature_score)
    VALUES(
      :word,
      :lemma,
      :tag,
      :gender,
      :plural,
      :syllable_count,
      :rhyme_end,
      :nature_score,
      :has_nature_score
    )
    """, words)
    conn.commit()
    return True
  except Exception as e:
    conn.rollback()
    error('Could not insert word {}'.format(e))
    return False

def make_where_part (parameters = [], allowed_columns = []):
  allowed_compares = ['<', '<=', '=', '>=', '>', '<>', '!=', 'BETWEEN']
  default_compare = '='
  filtered_parameters = {}
  where_chunks = []
  
  for row in parameters:
    column = row[0]

    if column in allowed_columns:
      if len(row) > 2:
        compare = row[1]
        if compare not in allowed_compares:
          compare = default_compare

        value = row[2]
      else:
        compare = default_compare
        value = row[1]

      if compare == 'BETWEEN':
        where_chunks.append('{column} {compare} {value}'.format(column=column, compare=compare, value=value))
      else:
        where_chunks.append('{column} {compare} :{column}'.format(column=column, compare=compare))
      filtered_parameters[column] = value

  if where_chunks:
    where_part = 'WHERE ' + ' AND '.join(where_chunks)
  else:
    where_part = None

  return (where_part, filtered_parameters)

"""
  'word': ('<', 1)
"""
def find_words (conn, parameters = [], limit=None):
  try:
    allowed_columns = ['word', 'lemma', 'tag', 'gender', 'plural', 'syllable_count', 'rhyme_end', 'nature_score', 'has_nature_score']
    where_part, parameters = make_where_part(parameters, allowed_columns)

    sql = 'SELECT * FROM words'

    if where_part:
      sql += ' ' + where_part

    sql += ' ORDER BY RANDOM()'

    if limit:
      sql += ' LIMIT {}'.format(limit)

    # debug(sql)

    cur = conn.cursor()
    cur.execute(sql, parameters)
    return cur.fetchall()
  except Exception as e:
    conn.rollback()
    error('Could not find words {}'.format(e))
    return False

def find_one_word (conn, parameters = []):
  try:
    allowed_columns = ['word', 'lemma', 'tag', 'syllable_count', 'rhyme_end', 'nature_score', 'has_nature_score']
    where_part, parameters = make_where_part(parameters, allowed_columns)

    sql = 'SELECT * FROM words'

    if where_part:
      sql += ' ' + where_part
    
    sql += ' ORDER BY RANDOM()'
    
    cur = conn.cursor()
    cur.execute(sql, parameters)
    return cur.fetchone()
  except Exception as e:
    conn.rollback()
    error('Could not find words {}'.format(e))
    return False