#!/usr/bin/python
# -*- coding: utf-8 -*-
import re
import string
import codecs
import glob
from pattern.fr import parsetree, singularize

outputfilename = 'data_pattern.txt'
input_patt = '../txt/*.txt'

def not_punctuation (word):
  return (word.type != '.' and word.type != ',' and word.type != '(' and word.type != ')' and word.type != 'LS' and word.type != 'SYM' and word.string != '—')

def flatten_sentence (sentence):
  return ' '.join([w.string for w in filter(not_punctuation, sentence.words)])
  # return ' '.join([w.string for w in sentence.words])

def flatten_tree (tree):
  return ' '.join(flatten_sentence(s) for s in tree.sentences)

with codecs.open(outputfilename, "w", "utf-8") as destination:
  for path in glob.glob(input_patt):
    print('** READING: ', path)
    
    with codecs.open(path, 'r', "utf-8") as source:
      tree = parsetree(source.read().lower())
      flat = flatten_tree(tree)
      destination.write(flat)
      destination.write('\n')
      # for sentence in parsed_text.sentences:
      #   lemmata = []

      #   for word in sentence.words:
      #     if word.type == 'NNS' or word.type == 'NNPS':
      #       # Test whether it's plural.
      #       lemmata.append(singularize(word.string))
      #     else:
      #       lemmata.append(word.lemma)

      #   destination.write(lemmata.join('\n') + '\n') 
      
    print ('*text is stripped*')
