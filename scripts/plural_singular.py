from pattern.fr import singularize, pluralize

#words = ['corne', 'coral', 'marbrier', 'nez', 'tombeau', 'nasal', 'idiot', 'escargot']

# find out if word is plural
def plurals(words):
	ending = ['s', 'x']
	plurals = []
	singulars = []
	for word in words:
		#print('\n')
		end = word[-1]
		print('end:', end)
		if end in ending:
			print('word is plural:', word)
			plurals.append(word)
		else:
			singulars.append(word)
	return plurals, singulars

# results = plurals(words)
# print(results)

# pluralize a word
def lots_of(words):
	pluralized = []
	for word in words:
		plural = pluralize(word)
		print('plural:', plural)
		pluralized.append(plural)
	return pluralized

# results = lots_of(words)
# print(results)

