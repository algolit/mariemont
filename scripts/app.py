"""
  API for the nature sonnet.
  - Allows to generate sonnets
  - Allows to plot them
"""

from flask import Flask, jsonify, request
from nature_sonnet import generate_nature_sonnet
from settings import BASEURL, HPGL_CACHEDIR, PLOT_PAGE, PLOT_HEADER_TEXT, PLOT_FOOTER_TEXT, PLOT_BODY_CHARACTER_SIZE, PLOT_FOOTER_CHARACTER_SIZE
import json
import random
from utils import hpgl_mm
from tempfile import mkstemp
import os.path
from os import makedirs
import time
from datetime import datetime
import re
import textwrap
from settings import THERMAL_CACHEDIR, THERMAL_FOOTER_TEXT, THERMAL_HEADER_TEXT

app = Flask(__name__)

if not os.path.exists(HPGL_CACHEDIR):
  makedirs(HPGL_CACHEDIR)

if not os.path.exists(THERMAL_CACHEDIR):
  makedirs(THERMAL_CACHEDIR)

@app.after_request
def add_headers(response):
  response.headers.add('Access-Control-Allow-Origin', '*')
  response.headers.add('Access-Control-Allow-Headers',
                        'Content-Type,Authorization')

  return response

@app.route("{}/generate".format(BASEURL), methods=["POST"])
def sonnet ():
  score = float(request.form['score'])

  return jsonify({ 'sonnet': generate_nature_sonnet(score), 'score': score })

"""
  Special chars for charset 12, with 17 as alternative
"""
def fix_special_char (c):
  if c == 'é':
    return chr(14) + chr(69) + chr(15)
  if c == 'É':
    return chr(14) + chr(93) + chr(15)
  if c == 'è':
    return chr(14) + chr(73) + chr(15)
  if c == 'È':
    return chr(14) + chr(35) + chr(15)
  if c == 'Ê':
    return chr(14) + chr (36) + chr(15)
  if c == 'ê':
    return chr(14) + chr (65) + chr(15)
  if c == 'ë':
    return chr(14) + chr (77) + chr(15)


  if c == 'á':
    return chr(14) + chr(68) + chr(15)
  if c == 'Á':
    return chr(14) + chr(96) + chr(15)
  if c == 'à':
    return chr(14) + chr(72) + chr(15)
  if c == 'À':
    return chr(14) + chr(33) + chr(15)
  if c == 'â':
    return chr(14) + chr(64) + chr(15)
  if c == 'Â':
    return chr(14) + chr(34) + chr(15)
  if c == 'Ä':
    return chr(14) + chr(88) + chr(15)

  if c == 'ó':
    return chr(14) + chr(70) + chr(15)
  if c == 'Ó':
    return chr(14) + chr(103) + chr(15)
  if c == 'ò':
    return chr(14) + chr(74) + chr(15)
  if c == 'Ò':
    return chr(14) + chr(104) + chr(15)
  if c == 'ô':
    return chr(14) + chr(66) + chr(15)
  if c == 'Ô':
    return chr(14) + chr(94) + chr(15)
  if c == 'ö':
    return chr(14) + chr(78) + chr(15)
  if c == 'Ö':
    return chr(14) + chr(78) + chr(15)
  if c == 'Õ':
    return chr(14) + chr(105) + chr(15)
  if c == 'õ':
    return chr(14) + chr(105) + chr(15)

  if c == 'û':
    return chr(14) + chr(67) + chr(15)
  if c == 'ú':
    return chr(14) + chr(71) + chr(15)
  if c == 'ù':
    return chr(14) + chr(75) + chr(15)
  if c == 'ü':
    return chr(14) + chr(79) + chr(15)
  if c == 'Ü':
    return chr(14) + chr(91) + chr(15)
  if c == 'Ù':
    return chr(14) + chr(45) + chr(15)
  if c == 'Ú':
    return chr(14) + chr(46) + chr(15)

  if c == 'í':
    return chr(14) + chr(85) + chr(15)
  if c == 'ï':
    return chr(14) + chr(93) + chr(15)
  if c == 'î':
    return chr(14) + chr(81) + chr(15)
  if c == 'Ï':
    return chr(14) + chr(39) + chr(15)
  if c == 'Î':
    return chr(14) + chr(38) + chr(15)

  if c == 'Ÿ':
    return chr(14) + chr(110) + chr(15)
  if c == 'ÿ':
    return chr(14) + chr(111) + chr(15)

  if c == 'ç':
    return chr(14) + chr(53) + chr(15)
  if c == 'Ç':
    return chr(14) + chr(52) + chr(15)

  if c == '«':
    return chr(14) + chr(123) + chr(15)
  if c == '»':
    return chr(14) + chr(125) + chr(15)

  return c

def plot_safe (dirty):
  safe = ''

  for c in dirty:
    safe += fix_special_char(c)

  return safe

@app.route("{}/plot".format(BASEURL), methods=["POST"])
def plot():
    score = float(request.form['score'])
    sonnet = request.form['sonnet']
    _, path = mkstemp(suffix='.hpgl', text=True, dir=HPGL_CACHEDIR)
    now = datetime.now()

    date = plot_safe(now.strftime('Créé le %d-%m-%Y à %H:%M'))

    with open(path, 'w') as h:
      hpgl = 'SP1;RO90;'
      hpgl += 'CS10;CA17;'
      hpgl += 'FS2;VS15;'
      hpgl += 'PA{},{};'.format(PLOT_PAGE['left'] + hpgl_mm(2), PLOT_PAGE['top'] - hpgl_mm(5))
      hpgl += 'SI{},{};'.format(*PLOT_FOOTER_CHARACTER_SIZE)
      hpgl += 'LB{}{};'.format(plot_safe(PLOT_HEADER_TEXT), chr(3))

      hpgl += 'PA{},{};'.format(PLOT_PAGE['left'] + hpgl_mm(2), PLOT_PAGE['top'] - hpgl_mm(25))
      hpgl += 'SI{},{};'.format(*PLOT_BODY_CHARACTER_SIZE)
      hpgl += 'LB{}{};'.format(plot_safe(sonnet), chr(3))
      
      hpgl += 'PA{},{};'.format(PLOT_PAGE['left'] + hpgl_mm(2), PLOT_PAGE['bottom'] + hpgl_mm(20))
      hpgl += 'SI{},{};'.format(*PLOT_FOOTER_CHARACTER_SIZE)
      hpgl += 'LBNature: {}\r\n{}\r\n{}{};'.format(score, date, plot_safe(PLOT_FOOTER_TEXT), chr(3))
      hpgl += 'SP0;'
      
      h.write(hpgl)
    
    return '100'

@app.route("{}/print".format(BASEURL), methods=["POST"])
def print():
  score = float(request.form['score'])
  sonnet, _ = re.subn(r'\s([!?])', r'\1', request.form['sonnet'])
  _, path = mkstemp(suffix='.txt', text=True, dir=THERMAL_CACHEDIR)

  with open(path, 'w') as h:
    h.write('\n')
    h.write(THERMAL_HEADER_TEXT)
    h.write('\n\n')
    for line in sonnet.split('\n'):
      h.write('\n'.join(textwrap.wrap(line, width=44, subsequent_indent='  ', break_on_hyphens=False)))
      h.write('\n')

    # h.write('\n'.join(sonnet_wrapped))
    h.write('\n\n')
    h.write(datetime.now().strftime('Créé le %d-%m-%Y à %H:%M\n'))
    h.write('Nature: {}\n'.format(score))
    h.write('\n')
    h.write(THERMAL_FOOTER_TEXT)

  return '100'