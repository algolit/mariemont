#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import chiplotle
import os.path
import time
import glob
from settings import HPGL_CACHEDIR
from shutil import move
from settings import VIRTUAL_PLOTTER
from os import makedirs
import codecs

PAPER_LOADED = 4
BUFFER_EMPTY = 8
NR_STATE = 32 # Not ready state

def get_state (plotter):
  # Should return current state
  plotter._serial_port.flushInput()
  plotter._serial_port.write('{}.O'.format(chr(27)))
  state = plotter._read_port()
  if state is not None:
    state = int(state.strip('\r\n'))
  return state

def get_pen_position (plotter):
  plotter._serial_port.flushInput()
  plotter._serial_port.write('OA')
  state = plotter._read_port()
  if state is not None:
    state = state.strip('\r\n')
  return state

if not os.path.exists(HPGL_CACHEDIR):
  makedirs(HPGL_CACHEDIR)

if not os.path.exists(os.path.join(HPGL_CACHEDIR, 'plotted')):
  makedirs(os.path.join(HPGL_CACHEDIR, 'plotted'))

def get_oldest_cachefile():
  files = [path for path in glob.glob(
    os.path.join(HPGL_CACHEDIR, '*.hpgl'))]
  if files:
    return sorted(files, cmp=lambda p, _: int(os.path.getmtime(p)), reverse=True)[0]
  else:
      return None

while True:
  try:
    if VIRTUAL_PLOTTER:
      plotter = chiplotle.tools.plottertools.instantiate_virtual_plotter()
    else:
      plotters = chiplotle.tools.plottertools.instantiate_plotters()
      if plotters:
        plotter = plotters[0]
      else:
        plotter = None

    # Not able to instantiate a plotter now. Trying again in 30 seconds.
    if not plotter:
      time.sleep(30)
      continue

    while True:
      cachefile = get_oldest_cachefile()
      if cachefile:
        print(cachefile)
        with codecs.open(cachefile, 'r', encoding='utf-8') as h:
          try:
            hpgl = h.read()
            plotter.write(u'IN;{};PG;'.format(hpgl).decode("utf-8").encode('ascii'))
            plotter._serial_port.flushInput()

            move(cachefile, os.path.join(HPGL_CACHEDIR, 'plotted', os.path.basename(cachefile)))

            if VIRTUAL_PLOTTER:
              chiplotle.tools.io.view(plotter)
              plotter = chiplotle.tools.plottertools.instantiate_virtual_plotter()
            else:
              state = 0
              while not state & BUFFER_EMPTY:
                state = get_state(plotter)
                print('Buffer not yet empty.')
                time.sleep(10)

              # last_position = None
              # current_position = get_pen_position(plotter)

              # while last_position <> current_position:
              #   last_position = current_position
              #   print(current_position)
              #   time.sleep(5)
              #   current_position = get_pen_position(plotter)


              # print('Pen stopped moving')

          except Exception as e:
            print('An error occured. Clearing plotter. Trying again in 30 seconds.')
            print(str(e))
            plotter = None
            break
            
          # except:
          #     print 'Something went wrong while plotting'
          #     pass

      time.sleep(1)
  except Exception as e:
    print('Could not initiate plotter, waiting one minute')
    print(str(e))
    time.sleep(60)