from settings import DEBUG

def debug (text):
  if DEBUG:
    print("Debug : {}".format(text))

def log (text):
  print("Log   : {}".format(text))

def error (text):
  print("Error : {}".format(text))

"""
  Convert length in mm to plotter units
"""
def hpgl_mm (l):
  return l * 40