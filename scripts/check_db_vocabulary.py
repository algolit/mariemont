from db import connect, find_one_word

"""
  Script to inspect database coverage for all words that can be replaced
"""

conn = connect()

with open('../data/queneau_words.txt', 'r') as source:
  for word in source:
    if not find_one_word(conn, [('tag', 'NOM'), ('word', word.strip())]):
      print('Can not find word: {}'.format(word.strip()))

      data = find_one_word(conn, [('word', word.strip())])

      if data:
        print('but found')
        for k in data.keys():
          print('{}: {}'.format(k, data[k]))
          
      print()