from nltk.corpus import stopwords

def stopwords(lang):
  return stopwords.words(lang)