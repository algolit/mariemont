#!/usr/bin/python -tt
# -*- coding: utf-8 -*-

from nltk.tokenize import word_tokenize
import re
import string
import codecs
import glob

outputfilename = 'data_nl.txt'
input_patt = 'txt/*.txt'

regex = re.compile('[%s]' % re.escape(string.punctuation)) #see documentation here: http://docs.python.org/2/library/string.html
toremove = ['”', '“', '«', '»']

with codecs.open(outputfilename, "w", "utf-8") as destination:
	for path in glob.glob(input_patt):
		print('** READING: ', path)
		sentences = []

		with codecs.open(path, 'r', "utf-8") as source:
			lines = source.readlines()
			if len(lines) == 1:
				lines = lines.split('. ')
			#print(lines)
			for line in lines:
				if line:
					for chr in toremove:
						line = line.replace(chr, '')

					for word in word_tokenize(line):
						word = word.strip()

						if word:
							destination.write(word + "\n")

		print ('*text is stripped*')
