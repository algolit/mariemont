# Loop through all epubfiles in subdirs and convert using ebook-convert
# https://stackoverflow.com/a/9612560
find . -name "*.epub" -type f -print0 | while read -d $'\0' path
do
  name=$(basename "${path}")
  newname="${name%.epub}.txt"

  echo "******"
  echo "Converting: ${path}"
  ebook-convert "${path}" "txt/${newname}"
done
