Installation for the exhibition 'Bye Bye Future', early 2020 in Mariemont.

Nature words: 2004
Technique words: 1989

Trained nature words: 1912
Trained technique words: 1868

Score classifier: 0.8783068783068783

Lexique vocabulary: 143089 words
Unscored words, not in corpus embeddings: 12258